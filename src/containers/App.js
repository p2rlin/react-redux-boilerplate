import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchItems} from '../store/actions';

class App extends Component {
    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(fetchItems());
    }

    getItems = () => {
        const {items, isFetching} = this.props;

        if (isFetching && items.length === 0) {
            return (<h2>Loading...</h2>);
        } else if (!isFetching && items.length === 0) {
            return (<h2>No items yet.</h2>)
        } else if (items.length > 0) {
            return (items.map((item,key) => <div key={key}>{item.item} from {item.shop} - {item.price}</div>));
        }
        console.log("No items");
        return "";
    };

    render() {
        return (
            <div>
                <h1>App</h1>
                <div>{this.getItems()}</div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const items = state.budgetItems.items || [];
    const isFetching = state.isFetching || true;

    return {items, isFetching};
}

export default connect(mapStateToProps)(App);