import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import configuration from '../store/configuration';

import App from '../containers/App';

const store = configuration();

export default class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={App}/>
                    </Switch>
                </BrowserRouter>
            </Provider>
        )
    }
}