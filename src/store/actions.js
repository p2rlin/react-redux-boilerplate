import axios from 'axios';
import {RECEIVE_BUDGET_ITEMS, REQUEST_BUDGET_ITEMS} from './constants';
import {getItems} from './mock-items';

const requestBudgetItems = () => {
    return {type: REQUEST_BUDGET_ITEMS};
};

const receiveBudgetItems = (json) => {
    return {type: RECEIVE_BUDGET_ITEMS, items: json};
};

export const fetchItems = () => {
    return dispatch => {
        dispatch(requestBudgetItems());
        return getItems()
            .then(items => dispatch(receiveBudgetItems(items)))
    }
};