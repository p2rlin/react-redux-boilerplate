import {combineReducers} from 'redux';
import {REQUEST_BUDGET_ITEMS, RECEIVE_BUDGET_ITEMS} from './constants';

const budgetItems = (state = {isFetching: false, items: []}, action) => {
    switch (action.type) {
        case REQUEST_BUDGET_ITEMS:
            return Object.assign({}, state, {isFetching: true});
        case RECEIVE_BUDGET_ITEMS:
            return Object.assign({}, state, {isFetching: false, items: action.items});
        default:
            return state;
    }
};

const rootReducer = combineReducers({budgetItems});

export default rootReducer;
